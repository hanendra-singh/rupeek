package com.hanendra.apiservices;

import com.hanendra.RestAssuredMethodsSetup;
import com.hanendra.utils.Constants;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

public class GetAllUsersApi {
    RestAssuredMethodsSetup restAssuredMethodsSetup=new RestAssuredMethodsSetup();

	
	 public Response GetAllUsersApiResponse(String token ){
	        String testUrl = RestAssured.baseURI + Constants.getAllusers;
	        return restAssuredMethodsSetup.getApi(testUrl, token);
	    }
}
