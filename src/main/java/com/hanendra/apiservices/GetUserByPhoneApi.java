package com.hanendra.apiservices;

import com.hanendra.RestAssuredMethodsSetup;
import com.hanendra.utils.Constants;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

public class GetUserByPhoneApi {

    RestAssuredMethodsSetup restAssuredMethodsSetup=new RestAssuredMethodsSetup();

	 public Response GetUserByPhoneResponse(String token, String number ){
	        String testUrl = RestAssured.baseURI + Constants.getAllusers+"/"+number;
	        return restAssuredMethodsSetup.getApi(testUrl, token);
	    }
}
