package com.hanendra.utils;

import org.json.simple.JSONObject;
import com.hanendra.RestAssuredMethodsSetup;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;


public class AuthUtil {
    RestAssuredMethodsSetup restAssuredMethodsSetup=new RestAssuredMethodsSetup();
    Response response;
    
    public Response TokenUsingCredentials(String username, String password) {

        JSONObject requestBody = new JSONObject();
        requestBody.put("username", username);
        requestBody.put("password", password);

        String testUrl = RestAssured.baseURI + Constants.Auth;
        
        response= restAssuredMethodsSetup.postApi(testUrl, requestBody.toString(), "application/json");
        return response;
    }
	
}
