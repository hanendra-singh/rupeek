package com.hanendra.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtils {
	private static Properties props = null;
	

	public static Properties loadProperties(String propFilePath){
		try{
			InputStream ins = new FileInputStream(new File(propFilePath));
			if (props == null) {
				props = new Properties();
				props.load(ins);
			}else{
				Properties properties = new Properties();
				properties.load(ins);
				props.putAll(properties);
				properties = null;
			}
			//Log.info("Loaded Properties file");
		}catch (Exception e){
			e.printStackTrace();
		}
		return props;
	}


}
