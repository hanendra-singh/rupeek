package com.hanendra.utils;

import java.util.Properties;


public class SettingConfig {

	public static String environment;
	public static String url;
	
	static String propertiesFile = "src/test/java/resources/config.properties";
	
	
	public static Properties config_properties = PropertiesUtils.loadProperties(propertiesFile);

	public static String propertyValue(String PropertyName) {
		return config_properties.getProperty(PropertyName);	
	}
	
	//For setting the Url globally
	public static void setUrl(String Url) {
		url = Url;
	}
	
	public static String getUrl() {
		return url;
	}

	//For setting the Environment globally
	public static void setEnvironment(String env) {
		environment = env;
	}
	public static String getEnvironment() {
		return environment;
	}
	
}
