package com.hanendra.tests;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.hanendra.BaseSetUp;
import com.hanendra.apiservices.GetAllUsersApi;
import com.hanendra.responsevalidators.GetAllUsersValidations;
import com.hanendra.utils.AuthUtil;
import com.hanendra.utils.Constants;
import com.jayway.restassured.response.Response;

public class GetAllUsersTest extends BaseSetUp{
	AuthUtil authUtil;
	GetAllUsersApi getAllUsersApi;
	GetAllUsersValidations getAllUsersValidations;
	String token;
    Logger logger;
    Response response1, response2;
    

	
	@BeforeClass()
	public void initMethod() {
		 authUtil=new AuthUtil();
		 getAllUsersApi=new GetAllUsersApi();
		 getAllUsersValidations=new GetAllUsersValidations();
		 token=authUtil.TokenUsingCredentials(Constants.username, Constants.password).getBody().jsonPath().getString("token");
		 logger=LoggerFactory.getLogger(this.getClass());
	}
	
	@Test
	public void getAllUsers(){
        logger.info("#### Running the Following Case Now-");				
        Response response = getAllUsersApi.GetAllUsersApiResponse(token);        
        logger.info("Response Body: {} \n Response code: {} \n Response Time {} ms", response.getBody().asString(), response.getStatusCode(), response.time());
        System.out.println("Response Body: {} \n Response code: {} \n Response Time {} ms"+ response.getBody().asString()+" "+ response.getStatusCode()+"  "+ response.time());

        Assert.assertEquals(response.getStatusCode(),200);
        
        //for more Assertions , pass the response in the validation class and put assertions-
        getAllUsersValidations.assertApiResponse(response);
		
		//assert response code
      //  assertResponseCode(response);
        
        //assert Header
       // assertHeader(response);
        
		//using jsonPath [can move this to either main class or a common parsing util)
       // com.jayway.restassured.path.json.JsonPath jsonPathEvaluator = response.jsonPath();
		//ArrayList<HashMap> employeeData = jsonPathEvaluator.getJsonObject("employeeData");

	}

}
