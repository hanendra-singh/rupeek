package com.hanendra.tests;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.hanendra.BaseSetUp;
import com.hanendra.apiservices.GetUserByPhoneApi;
import com.hanendra.responsevalidators.GetUserByPhoneValidations;
import com.hanendra.utils.AuthUtil;
import com.hanendra.utils.Constants;
import com.jayway.restassured.response.Response;

public class GetUserByPhoneTest extends BaseSetUp{
	AuthUtil authUtil;
	GetUserByPhoneApi getUserByPhoneApi;
	GetUserByPhoneValidations getUserByPhoneValidations;
	String token;
    Logger logger;
    Response response1, response2;
    
	
	@BeforeClass()
	public void initMethod() {
		 authUtil=new AuthUtil();
		 getUserByPhoneApi=new GetUserByPhoneApi();
		 getUserByPhoneValidations=new GetUserByPhoneValidations();
		 token=authUtil.TokenUsingCredentials(Constants.username, Constants.password).getBody().jsonPath().getString("token");
		 logger=LoggerFactory.getLogger(this.getClass());
	}
	
	//Just for testing i have taken a hard code phone number which i can also take from the response of previous api Dynamically-
	
	@Test
	public void getAllUsers(){
        logger.info("#### Running the Following Case Now-");				
        Response response = getUserByPhoneApi.GetUserByPhoneResponse(token,"8037602400");        
        logger.info("Response Body: {} \n Response code: {} \n Response Time {} ms", response.getBody().asString(), response.getStatusCode(), response.time());
        System.out.println("Response Body: {} \n Response code: {} \n Response Time {} ms"+ response.getBody().asString()+" "+ response.getStatusCode()+"  "+ response.time());

        Assert.assertEquals(response.getStatusCode(),200);
        
        //for more Assertions , pass the response in the validation class and put assertions-
        getUserByPhoneValidations.assertApiResponse(response);
		
		//using jsonPath [can move this to either main class or a common parsing util)
       // com.jayway.restassured.path.json.JsonPath jsonPathEvaluator = response.jsonPath();
		//ArrayList<HashMap> employeeData = jsonPathEvaluator.getJsonObject("employeeData");

	}
}

